﻿using UnityEngine;

public class NativeCC : MonoBehaviour
{
	private CharacterController cc;

	[SerializeField] private bool isAllowedToMove = false;

	[SerializeField] private bool isSimple;
	[SerializeField] private float maxSpeed = 5f;
	[SerializeField] private float acceleration = 10f;
	[SerializeField] private float friction = 5f;
	[SerializeField] private float jumpForce = 5f;
	[SerializeField] private Vector3 gravity = new Vector3(0f, -9.81f, 0f);
	private Vector3 velocity;

	#region Monobehaviour
	void Start()
	{
		cc = GetComponent<CharacterController>();
	}
	void Update()
	{
		if (isAllowedToMove)
			MovementInput();
	}
	#endregion

	#region 
	void MovementInput()
	{
		float vertical = Input.GetAxis("Vertical");
		float horizontal = Input.GetAxis("Horizontal");

		Vector3 moveDir = new Vector3(horizontal, 0f, vertical).normalized;

		Move(moveDir);
	}
	#endregion

	#region Apply Inputs
	void Move(Vector3 moveDir)
	{
		// move dependent on the active MovementController
		if (isSimple)       // Time.deltaTime is already included, so it´s not needed
		{
			velocity += moveDir * acceleration;
			velocity = Vector3.ClampMagnitude(velocity, maxSpeed);
			cc.SimpleMove(velocity);
			velocity -= velocity * friction;
		}
		else
		{
			velocity += moveDir * acceleration * Time.deltaTime;
			velocity = Vector3.ClampMagnitude(velocity, maxSpeed);

			if (cc.isGrounded)
				if (Input.GetButtonDown("Jump"))
					velocity.y = jumpForce;
				else
					velocity += gravity * Time.deltaTime;

			cc.Move(velocity * Time.deltaTime);
			velocity -= velocity * friction;
		}
	}
	#endregion
}